package ru.seschool.travel_blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.seschool.travel_blog.entities.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {

     Optional<User> findByUsername(String login);


}
