package ru.seschool.travel_blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.seschool.travel_blog.entities.Role;

public interface RoleRepository extends JpaRepository<Role,Long> {
}
