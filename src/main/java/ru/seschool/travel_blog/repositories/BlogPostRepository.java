package ru.seschool.travel_blog.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.seschool.travel_blog.entities.BlogPost;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public interface BlogPostRepository extends JpaRepository<BlogPost,Long> {

    public Iterable <BlogPost> findAllByShowOnMain(Boolean showOnMain);

    @Query ("from BlogPost bp where bp.title LIKE concat('%',?1,'%') OR bp.detailText LIKE concat('%',?1,'%') or bp.previewText LIKE concat('%',?1,'%')")
    public Page<BlogPost> searchPost(String query, PageRequest pageRequest);

    @Query("from BlogPost bp where bp.id in ?1")
    public Page<BlogPost> getListById(Iterable<Long> ids, PageRequest pageRequest);



}
