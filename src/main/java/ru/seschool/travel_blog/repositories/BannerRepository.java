package ru.seschool.travel_blog.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.seschool.travel_blog.entities.Banner;
import ru.seschool.travel_blog.entities.BannerType;

public interface BannerRepository extends CrudRepository<Banner, Long> {

    Banner findByType(BannerType type);

}
