package ru.seschool.travel_blog.Configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.seschool.travel_blog.services.UserService;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    protected UserService userService;

    //для кодирования пороля в БД
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().ignoringAntMatchers("/api/**").and()
              .authorizeRequests()
                .mvcMatchers("/admin/*").hasRole("ADMIN")
               // .mvcMatchers("/posts/*").hasAnyRole("ADMIN","READER")
                .mvcMatchers("/api/like/*").permitAll()
                .mvcMatchers("/api/*").authenticated()
                .anyRequest().permitAll()
             .and()

                .formLogin()
                .loginPage("/login/")
                .permitAll()
             .and()
                .logout()
                .logoutUrl("/logout/")
                .logoutSuccessUrl("/login/?logout")
              .and()
                .exceptionHandling().accessDeniedPage("/login/?accessError");
    }



    public void configureGlobal (AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.userService).passwordEncoder(this.bCryptPasswordEncoder());

    }
}
