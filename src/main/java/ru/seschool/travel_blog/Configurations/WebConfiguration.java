package ru.seschool.travel_blog.Configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Value("${server.root}")
    private String serverRoot;
    @Value("${upload.path}")
    private String uploadPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/upload/**")
                .addResourceLocations("file:///"+this.serverRoot+ this.uploadPath);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
       //если пришли в папку, то искать в ней файл .html
        registry.addViewController("*/").setViewName("forward:./index.html");
    }
}
