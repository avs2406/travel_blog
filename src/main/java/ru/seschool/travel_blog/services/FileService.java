package ru.seschool.travel_blog.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.seschool.travel_blog.entities.UploadResult;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
public class FileService {

    @Value("${upload.path}")
    private String uploadPath;
    @Value("${server.root}")
    private String serverRoot;


    public UploadResult saveFile(MultipartFile file){

        UploadResult result=new UploadResult();

        //по умолчанию всё плохо
        result.setSuccess(false);

        //если файл не пусьой
        if(!file.isEmpty()){

            //подготовить папку для сохранения файла
            File uploadDir=new File(this.serverRoot+this.uploadPath);

            if(!uploadDir.exists()){
                uploadDir.mkdir(); //создать, если пока нет
            }
            //сформировать уникальное имя
            String fileName= UUID.randomUUID() +file.getOriginalFilename();
            //положить файл в папку
            try {
                file.transferTo(new File(this.serverRoot+this.uploadPath +fileName));

                //заполнить результат , что всё хорошо
                result.setSuccess(true);
                result.setOriginalName(file.getOriginalFilename());
                result.setFileSrc((this.uploadPath+fileName).replace("\\","/"));
                result.setFileType(file.getContentType());



            } catch (IOException e) {
                e.printStackTrace();
            }


        }


        return result;

    }

}
