package ru.seschool.travel_blog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.seschool.travel_blog.entities.Banner;
import ru.seschool.travel_blog.entities.BannerType;
import ru.seschool.travel_blog.repositories.BannerRepository;

@Service
public class BannerService {

    @Autowired
    private BannerRepository bannerRepository;

    public Banner getMainBanner() {

        return this.bannerRepository.findByType(BannerType.MAIN);
    }

    public Banner save(Banner banner){
       return this.bannerRepository.save(banner);
    }

}
