package ru.seschool.travel_blog.services;

import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.util.ArrayList;

@Service
public class CookieService {

    /**
     * Получить значение куки по ключу (имени)
     * @param key название куки
     * @param cookies массив всех куки
     * @return значение куки (если куки не найдена - вернуть null)
     */
    public String getCookieByKey(String key, Cookie[] cookies) {
        if (cookies == null) {
            return null;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(key)) {
                return cookie.getValue();
            }
        }
        return null;
    }

    /**
     * Строковое значение куки с избранным сконвертировать в массив
     * @param strFavorites строковое значение куки со списком id постов
     * @return массив id постов
     */
    public ArrayList<Long> strFavoritesToArr(String strFavorites) {

        ArrayList<Long> result = new ArrayList<>();

        if (strFavorites == null) {
            return result;
        }

        String[] strFavIds = strFavorites.split("\\|");
        for (String strFavId : strFavIds) {
            try {
                result.add(Long.valueOf(strFavId));
            } catch (NumberFormatException err) {
                err.printStackTrace();
            }
        }

        return result;
    }

    /**
     * Массив id постов обернуть в куки
     * @param favorites массив id постов
     * @return объект куки, содержащий ID постов
     */
    public Cookie createFavoritesCookie(ArrayList<Long> favorites) {
        String value = "";
        for (Long favorite : favorites) {
            value += (favorite + "|");
        }

        Cookie result = new Cookie("favorites", value);
        result.setPath("/");
        result.setMaxAge(10 * 24 * 60 * 60);
        return result;
    }

}
