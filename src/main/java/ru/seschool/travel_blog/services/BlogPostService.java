package ru.seschool.travel_blog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.seschool.travel_blog.entities.BlogPost;
import ru.seschool.travel_blog.repositories.BlogPostRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class BlogPostService {


    public final int pageSize = 3;

    @Autowired
    private BlogPostRepository blogPostRepository;

    public Iterable<BlogPost> getMainBlogPosts() {
        return this.blogPostRepository.findAllByShowOnMain(true);
    }

    public Page<BlogPost> getPostsList(int page) {
        return this.blogPostRepository.findAll(PageRequest.of(page, this.pageSize));
    }

    public Page<BlogPost> getPostsList(Integer page, Integer pageSize) {
        return this.blogPostRepository.findAll(PageRequest.of(page, pageSize));
    }

    public Page<BlogPost> getPostsList(Integer page, Integer pageSize, String sort, String order) {

        Sort sortObj = Sort.by(sort);
        sortObj.ascending();
        if (order.equals("desc")) {
            sortObj = sortObj.descending();
        }

        return this.blogPostRepository.findAll(PageRequest.of(page, pageSize, sortObj));
    }

    public BlogPost getPostById(Long id) {

        Optional<BlogPost> blogPost = this.blogPostRepository.findById(id);
        if (blogPost.isPresent()) {
            return blogPost.get();
        }

        return null;
    }

    public Page<BlogPost> searchPost(String query, int page) {
        return this.blogPostRepository.searchPost(query, PageRequest.of(page, this.pageSize));

    }

    public BlogPost save(BlogPost post) {
        this.blogPostRepository.save(post);
        return post;
    }

    public Boolean deleteById(Long id) {
        try {
            this.blogPostRepository.deleteById(id);
            return true;
        } catch (Exception error) {
          error.printStackTrace();
        }
        return false;
    }

    public Page<BlogPost> getFavoritesList(Iterable<Long> ids, int page) {
        return this.blogPostRepository.getListById(ids,PageRequest.of(page, this.pageSize));
    }

}