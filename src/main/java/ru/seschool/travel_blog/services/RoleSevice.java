package ru.seschool.travel_blog.services;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;
        import ru.seschool.travel_blog.entities.Role;
        import ru.seschool.travel_blog.repositories.RoleRepository;

@Service
public class RoleSevice {

    @Autowired
    RoleRepository roleRepository;

    public Iterable<Role> getAllRoles() {
        return this.roleRepository.findAll();
    }

}
