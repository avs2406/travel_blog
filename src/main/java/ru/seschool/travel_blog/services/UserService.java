package ru.seschool.travel_blog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.seschool.travel_blog.entities.User;
import ru.seschool.travel_blog.repositories.UserRepository;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    //кодировщик пароля
    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        Optional<User> userOptional = this.userRepository.findByUsername(login);

        if (userOptional.isPresent()) {
            return userOptional.get();
        }

        throw new UsernameNotFoundException("пользователь не найден");
    }

    public Boolean addUser(User user) {
        if (user.getPassword().equals(user.getPasswordConfirm()) &&
                this.userRepository.findByUsername(user.getUsername()).isEmpty()) {
            user.setPassword(encoder.encode(user.getPassword()));
            this.userRepository.save(user);
            return true;
        }
        return false;
    }

    public Iterable<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    public User findById(Long id) {

        User result = null;

        Optional<User> userOptional = this.userRepository.findById(id);

        if (userOptional.isPresent()) {

            result = userOptional.get();
        }
        return result;
    }
    public User saveUser(User user){
        return this.userRepository.save(user);
    }



}
