package ru.seschool.travel_blog.entities;

import javax.persistence.*;

@Entity
public class BlogPost extends BaseEntity {

    private String title;
    private String previewText;

    @Lob
    private String detailText;
    private String previewImg;
    private String detailImg;
    private Boolean showOnMain;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPreviewText() {
        return previewText;
    }

    public void setPreviewText(String previewText) {
        this.previewText = previewText;
    }

    public String getDetailText() {
        return detailText;
    }

    public void setDetailText(String detailText) {
        this.detailText = detailText;
    }

    public String getPreviewImg() {
        return previewImg;
    }

    public void setPreviewImg(String previewImg) {
        this.previewImg = previewImg;
    }

    public String getDetailImg() {
        return detailImg;
    }

    public void setDetailImg(String detailImg) {
        this.detailImg = detailImg;
    }

    public Boolean getShowOnMain() {
        return showOnMain;
    }

    public void setShowOnMain(Boolean showOnMain) {
        this.showOnMain = showOnMain;
    }
}
