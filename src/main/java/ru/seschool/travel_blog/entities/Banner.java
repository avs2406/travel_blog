package ru.seschool.travel_blog.entities;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;

@Entity
public class Banner extends BaseEntity {

    private String imgName;
    private String bannerTitle;
    @Column(length = 5000)
    private String bannerText;

    private String buttonTitle;
    private String buttonLink;

    private BannerType type;

    @JsonGetter
    public String imageSrc(){
        //вернуть заглушку если картинка не задана
        if (this.imgName==null || this.imgName.isEmpty()){
            return "/images/nophoto.jpg";
        }
        //вернуть картинку
        return this.imgName;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public String getBannerText() {
        return bannerText;
    }

    public void setBannerText(String bannerText) {
        this.bannerText = bannerText;
    }

    public String getBannerTitle() {
        return bannerTitle;
    }

    public void setBannerTitle(String bannerTitle) {
        this.bannerTitle = bannerTitle;
    }

    public String getButtonTitle() {
        return buttonTitle;
    }

    public void setButtonTitle(String buttonTitle) {
        this.buttonTitle = buttonTitle;
    }

    public String getButtonLink() {
        return buttonLink;
    }

    public void setButtonLink(String buttonLink) {
        this.buttonLink = buttonLink;
    }

    public BannerType getType() {
        return type;
    }

    public void setType(BannerType type) {
        this.type = type;
    }
}
