package ru.seschool.travel_blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.seschool.travel_blog.entities.*;
import ru.seschool.travel_blog.services.*;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


@RestController
@RequestMapping("/api")
public class JsonController {

    @Autowired
    private BannerService bannerService;

    @Autowired
    private BlogPostService blogPostService;

    @Autowired
    private FileService fileService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleSevice roleSevice;

    @Autowired
    private  CookieService cookieService;

    @GetMapping("/main_banner/")
    public ResponseEntity<Banner> getMainBanner() {

        Banner banner = this.bannerService.getMainBanner();
        return ResponseEntity.ok().body(banner);
    }

    @PostMapping("/main_banner/")
    public ResponseEntity<Banner> saveMainBanner(Banner banner) {
        banner.setType(BannerType.MAIN);
        this.bannerService.save(banner);
        return ResponseEntity.ok(banner);
    }

    @PostMapping("/upload/image/")
    public ResponseEntity<UploadResult> saveImage(MultipartFile file) {
        UploadResult result = new UploadResult();
        result.setSuccess(false);
        if (file.getContentType().contains("image")) {
            //сохранить файл
            result = this.fileService.saveFile(file);
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/posts/")
    public ResponseEntity<Page<BlogPost>> getPosts(@RequestParam Integer page,
                                                   @RequestParam Integer pageSize,
                                                   @RequestParam(required = false, defaultValue = "id") String sort,
                                                   @RequestParam(required = false, defaultValue = "ask") String order) {
        Page<BlogPost> postsList = this.blogPostService.getPostsList(page, pageSize, sort, order);

        return ResponseEntity.ok(postsList);
    }

    @GetMapping("/posts/{id}/")
    public ResponseEntity<BlogPost> getById(@PathVariable Long id) {

        BlogPost postById = this.blogPostService.getPostById(id);


        if (postById != null) {
            return ResponseEntity.ok(postById);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/posts/")
    public ResponseEntity<BlogPost> addPost(BlogPost post) {

        this.blogPostService.save(post);

        return ResponseEntity.ok(post);
    }

    @PutMapping("/posts/{id}/")
    public ResponseEntity<BlogPost> updatedPost(BlogPost post) {
        this.blogPostService.save(post);
        return ResponseEntity.ok(post);
    }

    @DeleteMapping("/posts/{id}/")
    public ResponseEntity<BlogPost> deletePost(@PathVariable Long id) {

        Boolean result = this.blogPostService.deleteById(id);
        if (result == true) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/users/")
    public ResponseEntity<Iterable<User>> getAlUsers() {
        //получить всех пользователей
        Iterable<User> allUsers = this.userService.getAllUsers();
        return ResponseEntity.ok(allUsers);
    }

    @GetMapping("/roles/")
    public ResponseEntity<Iterable<Role>> getAllRoles() {
        Iterable<Role> allRoles = this.roleSevice.getAllRoles();
        return ResponseEntity.ok(allRoles);
    }

    @PostMapping("/users/")
    public ResponseEntity setUserRoles(Long userId
            , @RequestParam("roles") ArrayList<Long> roles) {
        User userForRoles = this.userService.findById(userId);

        Set<Role> rolesUser = new HashSet<>();

        for (Long role : roles) {

            Role tmp = new Role();
            tmp.setId(role);

            rolesUser.add(tmp);
        }

        userForRoles.setRoles(rolesUser);

        this.userService.saveUser(userForRoles);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/like/")
    public String addToFavorites(@RequestParam Long postId, HttpServletRequest request,
                                                            HttpServletResponse response){

        Cookie[] allCookies = request.getCookies();


        String strFavorites = this.cookieService.getCookieByKey("favorites", allCookies);

        ArrayList<Long> favorites = this.cookieService.strFavoritesToArr(strFavorites);

        String setLike;
        if (favorites.contains(postId)){

            favorites.remove(postId);
            setLike="0";
        }
        else{
            //добавить в массив postId
            favorites.add(postId);
            setLike="1";
        }

        Cookie favoritesCookie = this.cookieService.createFavoritesCookie(favorites);

        response.addCookie(favoritesCookie);
        return setLike;
    }

}
