package ru.seschool.travel_blog.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.seschool.travel_blog.entities.BlogPost;
import ru.seschool.travel_blog.services.BannerService;
import ru.seschool.travel_blog.services.BlogPostService;
import ru.seschool.travel_blog.services.CookieService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Controller
public class MainController {

    @Autowired
    private BannerService bannerService;

    @Autowired
    private BlogPostService blogPostService;

    @Autowired
    private CookieService cookieService;

   @ModelAttribute
    public void addFavoritesToModel(Model model, HttpServletRequest request){

        Cookie[] allCookies = request.getCookies();

        String strFavorites = this.cookieService.getCookieByKey("favorites", allCookies);

        ArrayList<Long> favorites = this.cookieService.strFavoritesToArr(strFavorites);

        model.addAttribute("favorites",favorites);//Массив favorites будет доступен во всех шаблонах
    }

    @GetMapping("/")
    public String showMain(Model model) {

        model.addAttribute("mainBanner", this.bannerService.getMainBanner());

        model.addAttribute("mainPosts", this.blogPostService.getMainBlogPosts());

        return "main";
    }

    @GetMapping("/posts/")
    public String showPostsList(Model model, @RequestParam(required = false, defaultValue = "1") int page) {

        Page<BlogPost> postsPage = this.blogPostService.getPostsList(page - 1);
        model.addAttribute("blogPostsPage", postsPage);

        model.addAttribute("pageNumbers", this.getNumbersForPagination(postsPage));
        return "posts";
    }

    @RequestMapping("/posts/{id}/")
    public String showPostDetail(Model model, @PathVariable String id) {

        BlogPost blogPost;

        try {
            Long postId = Long.parseLong(id);

            blogPost = this.blogPostService.getPostById(postId);
        } catch (Exception e) {
            return "redirect:/posts/";
        }


        if (blogPost != null) {
            model.addAttribute("postById", blogPost);
            return "detail";
        } else {
            return "redirect:/posts/";
        }
    }

    @GetMapping("/search/")
    public String showSearchResults(Model model,
                                    @RequestParam(required = false, defaultValue = "") String q,
                                    @RequestParam(required = false, defaultValue = "1") Integer page) {
        if (!q.equals("")) {
            Page<BlogPost> blogPosts = this.blogPostService.searchPost(q, page - 1);
            model.addAttribute("BlogPostsPage", blogPosts);
            model.addAttribute("pageNumbers", this.getNumbersForPagination(blogPosts));
            model.addAttribute("query", q);
        }
        return "search_result";
    }

    private ArrayList<Integer> getNumbersForPagination(Page page) {
        ArrayList<Integer> pageNumbers = new ArrayList<Integer>();
        for (int i = 1; i <= page.getTotalPages(); i++) {
            pageNumbers.add(i);
        }
        return pageNumbers;
    }

    @GetMapping("/favorites/")
    public String showFavoritesList(Model model, @RequestParam(required = false, defaultValue = "1") int page) {

        ArrayList<Long> favoriteIds = (ArrayList<Long>) model.getAttribute("favorites");


        Page<BlogPost> postsPage = this.blogPostService.getFavoritesList(favoriteIds,page - 1);
        model.addAttribute("blogPostsPage", postsPage);

        model.addAttribute("pageNumbers", this.getNumbersForPagination(postsPage));
        return "favorites";
    }

}
