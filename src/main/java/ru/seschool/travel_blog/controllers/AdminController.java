package ru.seschool.travel_blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.seschool.travel_blog.entities.Banner;
import ru.seschool.travel_blog.entities.BannerType;
import ru.seschool.travel_blog.services.BannerService;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Controller

@RequestMapping("/admin")
public class AdminController {


    @Autowired
    private BannerService bannerService;

    @Value("${upload.path}")
    private String uploadPath;
    @Value("${server.root}")
    private String serverRoot;

    @GetMapping("/main_banner/")
    public String showMainBannerForm(Model model) {

        Banner mainBanner = this.bannerService.getMainBanner();
        model.addAttribute("mainBanner", mainBanner);
        return "admin/main_banner";
    }

    @PostMapping("/main_banner/")
    public String saveMainBannerForm(Banner banner,
                                     @RequestParam(name = "imgFile") MultipartFile imgFile) throws IOException {

        if (!imgFile.isEmpty()) {

            if (imgFile.getContentType().contains("image")) {

                File uploadDir=new File(this.serverRoot+this.uploadPath);

                if(!uploadDir.exists()){
                    uploadDir.mkdir();
                }

                String imgName=UUID.randomUUID() +imgFile.getOriginalFilename();
                imgFile.transferTo(new File(this.serverRoot+this.uploadPath +imgName));
                banner.setImgName(this.uploadPath+imgName);
            }

        }

        banner.setType(BannerType.MAIN);
        this.bannerService.save(banner);
        return "redirect:/admin/main_banner/";
    }

}
