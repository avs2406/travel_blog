package ru.seschool.travel_blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.seschool.travel_blog.entities.User;
import ru.seschool.travel_blog.services.UserService;


@Controller
public class AuthorizeController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login/")
    public String loginForm(Model model,
                            @RequestParam(required = false) String error
                          , @RequestParam(required = false) String logout
                          , @RequestParam(required = false) String registration
                          , @RequestParam(required = false) String accessError){

        String massageClass = "success";
        String message = null;
        if (logout != null) {
            message = "Вы вышли из системы";
            massageClass = "success";
        }

        if (registration !=null){
            message ="Вы успешно зарегистрировались. Залогиньтесь";
            massageClass = "success";
        }

        if (accessError!=null){
            message = "У вас нет прав доступа";
            massageClass = "danger";
        }

        if (error != null) {
            message = "Неверно указаны логин или пароль";
            massageClass = "danger";
        }
        model.addAttribute("message", message);

        model.addAttribute("messageClass", massageClass);

        return "login";
    }

    @GetMapping("/registration/")
    public String showRegistration(Model model, @RequestParam(required = false) String passErr
            , @RequestParam(required = false) String existsErr) {

        String massageClass = "danger";
        String message = null;
        if (passErr != null) {
            message = "Пароль не совпадает";
        }
        if (existsErr != null) {
            message = "Существующий логин";
        }

        model.addAttribute("message", message);

        model.addAttribute("messageClass", massageClass);

        return "registration";
    }

    @PostMapping("/registration/")
    public String registrationUser(User user) {
        Boolean result = this.userService.addUser(user);
        if (result) {
            return "redirect:/login/?registration";
        }
        if (!result && !user.getPassword().equals(user.getPasswordConfirm())) {
            return "redirect:/registration/?passErr";
        }
        return "redirect:/registration/?existsErr";
    }
}
