console.log("like");

$(document).on('click', '.like', (e) => {
    const postId = $(e.target).data().postId;
    if (postId) {
        $.ajax("/api/like/?postId=" + postId,{
            success: (result) => {
                if (result == 1) {
                    $(e.target).addClass("active");
                }
                else {
                    $(e.target).removeClass("active");
                }
            },
        });
    }
});