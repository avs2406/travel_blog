window.ADMIN = {
    common: {

        MAIN_BANNER_LINK: "#banners-link",
        POSTS_LINK: "#posts-link",
        USERS_LINK: "#users-link",
        ADMIN_CONTENT: "#admin-content",
        ADMIN_PAGE_TITLE: ".admin-page-title",

        //Отправить ajax
        sendRequest: async function (url = '', data = null, method = "GET") {

            let json = data;
            if (data && typeof data.values == "function") {
                const object = {};
                data.forEach((value, key) => {
                    object[key] = value
                });
                json = object;
            }

            const info = {
                address: url,
                data: json,
                method, method,
            };
            console.log("Отправка запроса", info);
            const response = await fetch(url, {
                method: method,
                body: data
            });
            try {
                return await response.json();
            } catch (e) {
                return response;
            }
        },
    },

    main_banner: {

        MAIN_BANNER_FORM: "#main-banner-form",

        showBannerForm: function (bannerData = {}) {
            //Задать заголовок
            $(ADMIN.common.ADMIN_PAGE_TITLE).text("Баннер на главной");

            //Взять шаблон формы
            const bannerForm = $(ADMIN.main_banner.MAIN_BANNER_FORM + "-hidden").clone();
            bannerForm.attr("id", ADMIN.main_banner.MAIN_BANNER_FORM.replace(["#"], "")); //Поменять id

            //Наполнить форму данными с сервера
            bannerForm.find("input[name=bannerTitle]").val(bannerData.bannerTitle);
            bannerForm.find("textarea[name=bannerText]").text(bannerData.bannerText);
            bannerForm.find("input[name=buttonTitle]").val(bannerData.buttonTitle);
            bannerForm.find("input[name=buttonLink]").val(bannerData.buttonLink);
            bannerForm.find("input[name=imgName]").val(bannerData.imgName);
            bannerForm.find("input[name=id]").val(bannerData.id);
            bannerForm.find("img.main-banner-img").attr("src", bannerData.imageSrc);

            //Вставить на страницу
            $(ADMIN.common.ADMIN_CONTENT).html(bannerForm);
            bannerForm.show();
        },
    },
    posts: {

        POST_MODAL: "#post-edit-modal",
        POST_MODAL_HEADER: "#post-modal-header",
        POSTS_TABLE: "#posts-list-table",

        loadPostsList: async function (params) {
            if (params.data.limit) {
                params.data.pageSize = params.data.limit;
                params.data.page = (params.data.offset / params.data.limit).toFixed(0);
            }

            const posts = await ADMIN.common.sendRequest('/api/posts/' + '?' + $.param(params.data));
            if (posts && posts.content) {
                if (posts.content[0]) {
                    posts.content[0].checkForAction = true;
                }
                const result = {};
                result.total = posts.totalElements;
                result.totalNotFiltered = posts.totalElements;
                result.rows = posts.content;
                params.success(result);
            } else {
                alert("Ошибка загрузки списка постов");
            }
        },
        clearPostEditForm: function () {
            //Взять код модального окна
            const postEditForm = $(ADMIN.posts.POST_MODAL);
            //Спрятать картинки (для нового поста их нет)
            postEditForm.find(".img-admin-detail img").hide();
            postEditForm.find(".img-admin-preview img").hide();
            //Удалить из формы ID
            postEditForm.find("input[name=id]").val("");
            postEditForm.find("input[name=title]").val("");
            postEditForm.find("input[name=showOnMain]").prop("checked", false);
            postEditForm.find("textarea[name=previewText]").html("");
            postEditForm.find("input[name=previewImg]").val("");
            postEditForm.find("input[name=detailImg]").val("");
            postEditForm.find("input .custom-file-input").val("");
            CKEDITOR.instances['postPreviewText'].setData("");
            CKEDITOR.instances['postDetailText'].setData("");
        },
        fillFormWithPostData: function (postData) {
            const postEditForm = $(ADMIN.posts.POST_MODAL);

            postEditForm.find("input[name=id]").val(postData.id);
            postEditForm.find("input[name=title]").val(postData.title);
            CKEDITOR.instances['postPreviewText'].setData(postData.previewText);
            CKEDITOR.instances['postDetailText'].setData(postData.detailText);
            postEditForm.find("input[name=previewImg]").val(postData.previewImg);
            if (postData.previewImg) {
                postEditForm.find(".img-admin-preview img").attr("src", postData.previewImg);
                postEditForm.find(".img-admin-preview img").show();
            } else {
                postEditForm.find(".img-admin-preview img").hide();
            }
            postEditForm.find("input[name=detailImg]").val(postData.detailImg);
            if (postData.detailImg) {
                postEditForm.find(".img-admin-detail img").attr("src", postData.detailImg);
                postEditForm.find(".img-admin-detail img").show();
            } else {
                postEditForm.find(".img-admin-detail img").hide();
            }
            postEditForm.find("input[name=showOnMain]").prop('checked', postData.showOnMain);
        },
        buttons: function () {
            return {
                btnAdd: {
                    text: 'Добавить пост',
                    icon: 'fa-plus',
                    event: function () {
                        //Почистить форму от предыдущих заполнений
                        ADMIN.posts.clearPostEditForm();
                        //Взять код модального окна
                        const postEditForm = $(ADMIN.posts.POST_MODAL);
                        //Задать заголовок
                        $(ADMIN.posts.POST_MODAL_HEADER).html("Добавить пост");
                        //Показать окно
                        postEditForm.modal("show");
                    },
                    attributes: {
                        title: 'Добавить новый пост'
                    }
                },
                btnEdit: {
                    text: 'Редактировать пост',
                    icon: 'fa-edit',
                    event: async function () {
                        //ID поста на редактирование
                        const postId = $(ADMIN.posts.POSTS_TABLE).find("input[name=postId]:checked").val();

                        //Взять код модального окна
                        const postEditForm = $(ADMIN.posts.POST_MODAL);
                        //Запросить данные о посте
                        const postData = await ADMIN.common.sendRequest('/api/posts/' + postId + '/');

                        if (postData.id) {
                            //Проставить в окно данные о посте
                            ADMIN.posts.fillFormWithPostData(postData);
                            //Задать заголовок
                            $(ADMIN.posts.POST_MODAL_HEADER).html("Редактировать пост");
                            //Показать окно
                            postEditForm.modal("show");
                        } else {
                            alert("Ошибка получения данных");
                        }

                    },
                    attributes: {
                        title: 'Редактировать отмеченный'
                    }
                },
                btnDelete: {
                    text: 'Удалить пост',
                    icon: 'fa-trash',
                    event: async function () {
                        const postId = $(ADMIN.posts.POSTS_TABLE).find("input[name=postId]:checked").val();
                        if (confirm("Вы уверены, что хотите удалить пост?")) {
                            const result = await ADMIN.common.sendRequest("/api/posts/" + postId + "/", null, "DELETE");
                            if (result.ok) {
                                $(ADMIN.posts.POSTS_TABLE).bootstrapTable("refresh");
                            } else {
                                alert("Ошибка удаления");
                            }
                        }
                    },
                    attributes: {
                        title: 'Удалить отмеченный'
                    }
                },
            };
        }
    },
    users: {

        USER_SAVE_BTN: ".save-user",
        USER_ROLES_SELECT: ".user-roles",

        getUserHtml: (userData, allRoles) => {
            let result = $("<div>" +
                "<div class='row'>" +
                "<div class='col-4'><b>username</b></div>" +
                "<div class='col-4'><b>email</b></div>" +
                "<div class='col-4'><b>roles</b></div>" +
                "</div></div>");

            if (Array.isArray(userData)) {
                userData.forEach((user) => {
                    const row = $("<div class='row'/>");
                    row.append("<div class='col-4'/>" + user.username);
                    row.append("<div class='col-4'/>" + user.email);
                    row.append($("<div class='col-2'/>").append(ADMIN.users.getRoleSelect(allRoles, user.roles).attr('data-user', user.id)));
                    row.append($("<div class='col-2'/>")
                        .append($("<button class='btn btn-primary " + ADMIN.users.USER_SAVE_BTN.replace(".", "") + "'>ок</button>")
                            .attr("data-user", user.id)));
                    result.append(row);
                    result.append("<hr>");
                });
            }

            return result;
        },

        getRoleSelect: (allRoles, selectedRoles) => {
            let select = $("<select class='form-control " + ADMIN.users.USER_ROLES_SELECT.replace(".", "") + "' multiple/>");

            allRoles.forEach(function (role) {
                const option = $("<option/>").attr("value", role.id).text(role.name);
                if (selectedRoles.find(cur => cur.id == role.id)) {
                    option.attr('selected', true);
                }
                select.append(option);
            });

            return select;
        },
    }
};

$(ADMIN.common.MAIN_BANNER_LINK).on('click', async (e) => {
    //Запросить баннер
    const bannerData = await ADMIN.common.sendRequest("/api/main_banner/");
    ADMIN.main_banner.showBannerForm(bannerData);
});


$(ADMIN.common.POSTS_LINK).on('click', (e) => {

    //Задать заголовок
    $(ADMIN.common.ADMIN_PAGE_TITLE).text("Список постов");
    //Создать пустую таблицу
    $(ADMIN.common.ADMIN_CONTENT).html(
        "<table id='" + ADMIN.posts.POSTS_TABLE.replace(["#"], "") + "'></table>"
    );
    //Инициализировать bootstrap table
    $(ADMIN.posts.POSTS_TABLE).bootstrapTable({
        ajax: "ADMIN.posts.loadPostsList", //функция для запроса на сервер
        pagination: true,
        sidePagination: 'server',
        locale: 'ru-RU',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        showFullscreen: true,
        buttons: "ADMIN.posts.buttons",
        idField: "id",
        selectItemName: "postId",
        silentSort: false,
        columns: [
            {
                radio: true,
                field: 'checkForAction',
                title: "Отметить"
            },
            {
                field: 'id',
                title: 'ID',
                sortable: true,
            }, {
                field: 'title',
                title: 'Заголовок',
                sortable: true,
            },
            {
                field: 'dateCreate',
                title: 'Дата создания',
                sortable: true,
            },
            {
                field: 'dateUpdate',
                title: 'Дата изменения',
                sortable: true,
            }]
    });
});

$(ADMIN.common.USERS_LINK).on('click', async (e) => {
    //Задать заголовок
    $(ADMIN.common.ADMIN_PAGE_TITLE).text("Список пользователей");

    //Запросить пользователей
    const usersData = await ADMIN.common.sendRequest("/api/users/");
    if (usersData.error) {
        alert("Ошибка загрузки списка пользователей");
        return;
    }
    //Запросить список всех ролей
    const allRoles = await ADMIN.common.sendRequest("/api/roles/");
    if (allRoles.error) {
        alert("Ошибка загрузки списка ролей");
        return;
    }

    //Показать полученных пользователей
    $(ADMIN.common.ADMIN_CONTENT).html(
        ADMIN.users.getUserHtml(usersData, allRoles)
    );

});

//Сохранить данные пользователя
$(document).on('click', ADMIN.users.USER_SAVE_BTN, async (e) => {
    const userId = $(e.target).data().user;

    const $select = $(ADMIN.users.USER_ROLES_SELECT + '[data-user=' + userId + ']');
    const selectedRoles = $select.val();

    const data = new FormData();
    data.append('userId', userId);
    selectedRoles.forEach((role, index) => {
        data.append(`roles`, role);
    });
//data.append('roles', selectedRoles);
    const response = await ADMIN.common.sendRequest('/api/users/', data, 'POST');

});

$(document).on('submit', ADMIN.main_banner.MAIN_BANNER_FORM, async (e) => {
    e.preventDefault();
    const fData = new FormData(e.target);
    const bannerData = await ADMIN.common.sendRequest("/api/main_banner/", fData, "POST");

    if (bannerData && bannerData.id) {
        ADMIN.main_banner.showBannerForm(bannerData);
    } else {
        alert("Ошибка загрузки формы");
    }

});

$(document).on('submit', '#post-edit-form', async (e) => {
    e.preventDefault();
    const fData = new FormData(e.target);
    const postId = fData.get("id");
    let url = "/api/posts/";
    let method = "POST";

    if (postId) {
        url += postId + "/";
        method = "PUT";
    }

    const response = await ADMIN.common.sendRequest(url, fData, method);

    if (response.id) {
        alert("Форма успешно сохранена");
        ADMIN.posts.fillFormWithPostData(response);
        $(ADMIN.posts.POSTS_TABLE).bootstrapTable("refresh");
    } else {
        alert("Ошибка загрузки формы");
    }

});

$(document).on('change', 'input[type="file"].custom-file-input', async function (e) {
    const fileInput = $(e.target);
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append("file", file);
    const result = await ADMIN.common.sendRequest("/api/upload/" + fileInput.data().fileType + "/",
        formData, "POST");

    if (result.success) {
        if (fileInput.data().fileType == 'image')
            $(fileInput.data().targetImg).attr("src", result.fileSrc).show();
        $(fileInput.data().targetInput).val(result.fileSrc);
    } else {
        alert("Ошибка загрузки файла");
    }
});

//ПРИМЕРЫ
window.EXAMPLES = {
    getBanner: {
        url: "/api/main_banner/",
        answer: {
            "id": 5,
            "dateCreate": "01.10.2020 10:46:57",
            "dateUpdate": "01.10.2020 11:46:57",
            "imgName": "",
            "bannerTitle": "Заголовок",
            "bannerText": "Текст!",
            "buttonTitle": "перейти куда то",
            "buttonLink": "/posts/",
            "type": "MAIN",
            "imageSrc": "/images/nophoto.jpg"
        }
    },
    getPosts: {
        url: "/api/posts/?sort=title&order=asc&offset=0&limit=10&pageSize=10&page=0",
        answer: {
            "content": [{
                "id": 1,
                "dateCreate": null,
                "dateUpdate": null,
                "title": "кул имейдж",
                "previewText": "для задания с клёвой картинкой",
                "detailText": "для задания с клёвой картинкой",
                "previewImg": "https://klike.net/uploads/posts/2019-08/1566721808_1.jpg",
                "detailImg": "https://klike.net/uploads/posts/2019-08/1566721808_1.jpg",
                "showOnMain": false
            }, {
                "id": 2,
                "dateCreate": null,
                "dateUpdate": null,
                "title": "котики в пути",
                "previewText": "блог с котиками",
                "detailText": "блог с котиками",
                "previewImg": "https://klike.net/uploads/posts/2019-08/1566721879_7.jpg",
                "detailImg": "https://klike.net/uploads/posts/2019-08/1566721879_7.jpg",
                "showOnMain": true
            }, {
                "id": 3,
                "dateCreate": null,
                "dateUpdate": null,
                "title": "собаки догоняют котов-безпредельников",
                "previewText": "собаки в погоне за котиками",
                "detailText": "собаки в погоне за котиками",
                "previewImg": "https://infokava.com/uploads/posts/2016-07/1468915821_gl40.jpg",
                "detailImg": "https://infokava.com/uploads/posts/2016-07/1468915821_gl40.jpg",
                "showOnMain": true
            }],
            "pageable": {
                "sort": {"sorted": true, "unsorted": false, "empty": false},
                "offset": 0,
                "pageNumber": 0,
                "pageSize": 10,
                "unpaged": false,
                "paged": true
            },
            "totalPages": 1,
            "last": true,
            "totalElements": 10,
            "size": 10,
            "number": 0,
            "numberOfElements": 10,
            "sort": {"sorted": true, "unsorted": false, "empty": false},
            "first": true,
            "empty": false
        }
    },
    getPost: {
        url: "/api/posts/1/",
        answer: {
            "id": 1,
            "dateCreate": "01.10.2020 11:53:59",
            "dateUpdate": "01.10.2020 11:53:59",
            "title": "кул имейдж",
            "previewText": "<p>для задания с клёвой картинкой</p>\r\n",
            "detailText": "<p>для задания с клёвой картинкой</p>\r\n",
            "previewImg": "https://klike.net/uploads/posts/2019-08/1566721808_1.jpg",
            "detailImg": "https://klike.net/uploads/posts/2019-08/1566721808_1.jpg",
            "showOnMain": false
        }
    }
};